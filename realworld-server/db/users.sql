CREATE TABLE IF NOT EXISTS users (
  id integer PRIMARY KEY autoincrement not null,
  email char(20) not null,
  token char(50),
  username char(20) not null,
  bio text,
  image text,
  follows text,
  password char(20) not null
);
