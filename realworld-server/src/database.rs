use rocket_db_pools::{sqlx, Database};

#[derive(Database)]
#[database("real_world")]
pub struct Db(sqlx::SqlitePool);
