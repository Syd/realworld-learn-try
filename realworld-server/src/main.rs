mod api;
mod database;

use api::profiles::{profile_follow, profile_get, profile_unfollow};
use api::user::{user_get, user_login, user_regist, user_update};
use database::Db;

#[macro_use]
extern crate rocket;

use rocket::{fs::relative, fs::FileServer, launch, routes};
use rocket_db_pools::Database;

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Db::init())
        .mount("/", FileServer::from(relative!("static")))
        .mount(
            "/api",
            routes![user_login, user_regist, user_get, user_update],
        )
        .mount(
            "/api",
            routes![profile_get, profile_follow, profile_unfollow],
        )
}
