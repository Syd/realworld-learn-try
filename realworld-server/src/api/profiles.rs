mod error;
mod follow;
mod get;
mod unfollow;

pub use follow::follow as profile_follow;
pub use get::get as profile_get;
pub use unfollow::unfollow as profile_unfollow;

use rocket::serde::Serialize;

#[derive(Debug, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Profile<T> {
    pub profile: T,
}

#[derive(Debug, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Base {
    username: String,
    bio: String,
    image: String,
    following: bool,
}
