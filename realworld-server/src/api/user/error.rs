#[derive(Debug, Responder)]
pub enum UserError {
    #[response(status = 500)]
    EmailExist(String),
    #[response(status = 500)]
    LoginFaile(String),
    #[response(status = 500)]
    JwtParse(String),
    #[response(status = 500)]
    Unexpected(String),
    #[response(status = 505)]
    AuthError(String),
    #[response(status = 404)]
    Sqlx(String),
}

impl From<sqlx::Error> for UserError {
    fn from(e: sqlx::Error) -> Self {
        Self::Sqlx(e.to_string())
    }
}

impl From<jsonwebtoken::errors::Error> for UserError {
    fn from(e: jsonwebtoken::errors::Error) -> Self {
        Self::JwtParse(e.to_string())
    }
}
