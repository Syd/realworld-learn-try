use rocket::serde::json::Json;
use rocket_db_pools::Connection;

use crate::database::Db;

use super::{auth::AuthClaims, error::UserError, Base, User};

// TODO: 检查密钥是否过时
#[get("/user")]
pub async fn current(
    auth: AuthClaims,
    mut db: Connection<Db>,
) -> Result<Json<User<Base>>, UserError> {
    Base::query_by_email(&mut *db, &auth.email)
        .await
        .map(|user| Json(User { user }))
        .map_err(|e| UserError::Sqlx(e.to_string()))
}
