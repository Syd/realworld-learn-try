use chrono::Duration;
use rocket::serde::{json::Json, Deserialize};
use rocket_db_pools::Connection;

use crate::{
    api::user::{auth::AuthClaims, error::UserError, Base, User},
    database::Db,
};

#[derive(Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct Login {
    email: String,
    password: String,
}

#[post("/users/login", format = "json", data = "<login_user>")]
pub async fn login(
    login_user: Json<User<Login>>,
    mut db: Connection<Db>,
) -> Result<Json<User<Base>>, UserError> {
    let (password,): (String,) = sqlx::query_as("SELECT password FROM users WHERE email = ?")
        .bind(&login_user.user.email)
        .fetch_one(&mut *db)
        .await
        .map_err(|e| UserError::LoginFaile(format!("email wrong !\n{e}")))?;

    if password == login_user.user.password {
        let user = Base::query_by_email(&mut *db, &login_user.user.email).await?;

        let token = AuthClaims::new(String::from("User"), Duration::weeks(1), user.email.clone())
            .encode()?;

        sqlx::query("UPDATE users SET token = ? WHERE email = ?")
            .bind(&token)
            .bind(&user.email)
            .execute(&mut *db)
            .await?;

        Ok(Json(User {
            user: Base { token, ..user },
        }))
    } else {
        Err(UserError::LoginFaile("password wrong !".to_string()))
    }
}
