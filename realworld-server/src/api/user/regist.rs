use chrono::Duration;
use rocket::serde::{json::Json, Deserialize};
use rocket_db_pools::Connection;

use crate::{
    api::user::{error::UserError, Base, User},
    database::Db,
};

use super::auth::AuthClaims;

#[derive(Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct Regist {
    username: String,
    email: String,
    password: String,
}

#[post("/users", format = "json", data = "<register>")]
pub async fn regist(
    register: Json<User<Regist>>,
    mut db: Connection<Db>,
) -> Result<Json<User<Base>>, UserError> {
    if let Ok((email,)) = sqlx::query_as::<_, (String,)>("SELECT email from users where email = ?")
        .bind(&register.user.email)
        .fetch_one(&mut *db)
        .await
    {
        eprintln!("email exist {email}");
        Err(UserError::EmailExist(email))
    } else {
        let token = AuthClaims::new(
            String::from("user"),
            Duration::weeks(1),
            register.user.email.clone(),
        )
        .encode()?;

        sqlx::query("INSERT INTO users (email,token,username,password) VALUES (?,?,?,?)")
            .bind(&register.user.email)
            .bind(&token)
            .bind(&register.user.username)
            .bind(&register.user.password)
            .execute(&mut *db)
            .await?;

        Ok(Json(User {
            user: Base {
                email: register.user.email.clone(),
                token,
                username: register.user.username.clone(),
                bio: "".to_string(),
                image: "".to_string(),
            },
        }))
    }
}
