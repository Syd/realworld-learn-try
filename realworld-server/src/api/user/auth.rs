use chrono::{Duration, Utc};
use jsonwebtoken::{DecodingKey, EncodingKey, Header, Validation};
use once_cell::sync::Lazy;
use rocket::http::Status;
use rocket::request::FromRequest;
use rocket::{
    request::Outcome,
    serde::{Deserialize, Serialize},
};

use super::error::UserError;

const ENCODEKEY: Lazy<EncodingKey> = Lazy::new(|| EncodingKey::from_secret(b"secret"));
const DECODEKEY: Lazy<DecodingKey> = Lazy::new(|| DecodingKey::from_secret(b"secret"));

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct AuthClaims {
    pub sub: String,
    pub jat: i64,
    pub exp: i64,
    pub email: String,
}

impl AuthClaims {
    pub fn new(sub: String, duration: Duration, email: String) -> Self {
        let jat_date = Utc::now();
        Self {
            sub,
            jat: jat_date.timestamp(),
            exp: (jat_date + duration).timestamp(),
            email,
        }
    }

    pub fn encode(&self) -> Result<String, jsonwebtoken::errors::Error> {
        jsonwebtoken::encode(&Header::default(), self, &ENCODEKEY)
    }

    pub fn decode(token: &str) -> Result<Self, jsonwebtoken::errors::Error> {
        jsonwebtoken::decode::<Self>(token, &DECODEKEY, &Validation::default())
            .map(|data| data.claims)
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthClaims {
    type Error = UserError;

    // TODO: Fix these error handles!
    async fn from_request(requst: &'r rocket::Request<'_>) -> Outcome<Self, Self::Error> {
        if let Some(header_auth) = requst.headers().get_one("Authorization") {
            let mut header_split = header_auth.split_whitespace();

            if header_split.next() == Some("Token") {
                if let Some(token) = header_split.next() {
                    match AuthClaims::decode(token) {
                        Ok(claims) => Outcome::Success(claims),
                        Err(e) => Outcome::Failure((
                            Status::Unauthorized,
                            UserError::AuthError(format!("Error when decode token\n{e}")),
                        )),
                    }
                } else {
                    Outcome::Failure((
                        Status::Unauthorized,
                        UserError::AuthError(String::from("Not found token !")),
                    ))
                }
            } else {
                Outcome::Failure((
                    Status::Unauthorized,
                    UserError::AuthError(String::from(
                        "token format wrong !\nThere is no 'token' segment !",
                    )),
                ))
            }
        } else {
            Outcome::Failure((
                Status::Unauthorized,
                UserError::AuthError(String::from("Didn't Found Authorization !")),
            ))
        }
    }
}
