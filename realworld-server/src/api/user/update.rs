use std::collections::HashMap;

use rocket::serde::json::Json;
use rocket_db_pools::Connection;

use crate::database::Db;

use super::{auth::AuthClaims, error::UserError, Base, User};

// #[derive(Debug, Deserialize)]
// #[serde(crate = "rocket::serde")]
// struct Update {
//     email: Option<String>,
//     username: Option<String>,
//     password: Option<String>,
//     image: Option<String>,
//     bio: Option<String>,
// }

#[put("/user", format = "json", data = "<user_data>")]
pub async fn update(
    user_data: Json<User<HashMap<String, String>>>,
    auth: AuthClaims,
    mut db: Connection<Db>,
) -> Result<Json<User<Base>>, UserError> {
    dbg!(&user_data);

    let set_part = user_data
        .user
        .clone()
        .into_iter()
        .map(|(key, value)| format!("{key} = \"{value}\""))
        .reduce(|prev, now| prev + ", " + &now)
        .ok_or(UserError::Unexpected(format!(
            "Update Data: {:?}",
            &user_data
        )))?;

    dbg!(&set_part);

    sqlx::query(&format!("UPDATE users SET {set_part} WHERE email = ?"))
        .bind(&auth.email)
        .execute(&mut *db)
        .await?;
    dbg!("update success !");

    let key = user_data.user.keys().next().unwrap();
    let user: Base = sqlx::query_as(&format!(
        "SELECT email,token,username,bio,image FROM users WHERE {0} = ?",
        key
    ))
    .bind(user_data.user.get(key))
    .fetch_one(&mut *db)
    .await?;
    dbg!(&user);

    Ok(Json(User { user }))
}
