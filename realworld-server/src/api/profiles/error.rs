#[derive(Debug, Responder)]
pub enum ProfileError {
    #[response(status = 500)]
    Placeholder(String),
    #[response(status = 404)]
    Sqlx(String),
}

impl From<sqlx::error::Error> for ProfileError {
    fn from(e: sqlx::error::Error) -> Self {
        Self::Sqlx(e.to_string())
    }
}
