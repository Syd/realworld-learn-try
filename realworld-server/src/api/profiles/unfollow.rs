use rocket::serde::json::Json;
use rocket_db_pools::Connection;

use crate::{
    api::{profiles::Profile, user::auth::AuthClaims},
    database::Db,
};

use super::{error::ProfileError, Base};

#[delete("/profiles/<username>/follow")]
pub async fn unfollow(
    username: String,
    auth: AuthClaims,
    mut db: Connection<Db>,
) -> Result<Json<Profile<Base>>, ProfileError> {
    let (follows,): (String,) = sqlx::query_as("SELECT follows FROM users WHERE email = ?")
        .bind(&auth.email)
        .fetch_one(&mut *db)
        .await?;

    let follows = follows
        .split(", ")
        .filter(|&follow| follow != &username)
        .collect::<Vec<_>>()
        .join(", ");

    dbg!(&follows);

    sqlx::query("UPDATE users SET follows = ? WHERE email = ?")
        .bind(follows)
        .bind(&auth.email)
        .execute(&mut *db)
        .await?;

    let (bio, image): (String, String) =
        sqlx::query_as("SELECT bio, image FROM users WHERE username = ?")
            .bind(&username)
            .fetch_one(&mut *db)
            .await?;

    Ok(Json(Profile {
        profile: Base {
            username,
            bio,
            image,
            following: false,
        },
    }))
}
