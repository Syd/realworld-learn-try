use rocket::serde::json::Json;
use rocket_db_pools::Connection;

use crate::{
    api::{profiles::Profile, user::auth::AuthClaims},
    database::Db,
};

use super::{error::ProfileError, Base};

#[post("/profiles/<username>/follow")]
pub async fn follow(
    username: String,
    auth: AuthClaims,
    mut db: Connection<Db>,
) -> Result<Json<Profile<Base>>, ProfileError> {
    let (follows,): (String,) = sqlx::query_as("SELECT follows FROM users WHERE email = ?")
        .bind(&auth.email)
        .fetch_one(&mut *db)
        .await?;

    let mut follows = if follows == "" {
        vec![]
    } else {
        follows.split(", ").collect::<Vec<_>>()
    };

    dbg!(&follows);

    if !follows.contains(&username.as_str()) {
        follows.push(&username);
    }

    let follows = follows.join(", ");

    // TODO: Check whether follows is empty ?
    sqlx::query("UPDATE users SET follows = ? WHERE email = ?")
        .bind(&follows)
        .bind(&auth.email)
        .execute(&mut *db)
        .await?;

    dbg!(&follows);

    let (bio, image): (String, String) =
        sqlx::query_as("SELECT bio, image FROM users WHERE username = ?")
            .bind(&username)
            .fetch_one(&mut *db)
            .await?;

    Ok(Json(Profile {
        profile: Base {
            username,
            bio,
            image,
            following: true,
        },
    }))
}
