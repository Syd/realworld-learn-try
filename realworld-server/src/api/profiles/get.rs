use rocket::serde::json::Json;
use rocket_db_pools::Connection;

use crate::{
    api::{
        profiles::{error::ProfileError, Base, Profile},
        user::auth::AuthClaims,
    },
    database::Db,
};

#[get("/profiles/<username>")]
pub async fn get(
    username: String,
    auth: Option<AuthClaims>,
    mut db: Connection<Db>,
) -> Result<Json<Profile<Base>>, ProfileError> {
    let following = if let Some(auth) = auth {
        let (follows,): (String,) = sqlx::query_as("SELECT follows FROM users WHERE email = ?")
            .bind(&auth.email)
            .fetch_one(&mut *db)
            .await?;
        dbg!(auth);
        let follows = follows.split(", ").collect::<Vec<&str>>();
        if follows.contains(&username.as_str()) {
            true
        } else {
            false
        }
    } else {
        false
    };

    let (bio, image): (String, String) =
        sqlx::query_as("SELECT bio,image FROM users WHERE username = ?")
            .bind(&username)
            .fetch_one(&mut *db)
            .await?;
    dbg!((&bio, &image));
    Ok(Json(Profile {
        profile: Base {
            username,
            bio,
            image,
            following,
        },
    }))
}
