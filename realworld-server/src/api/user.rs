pub mod auth;
mod current;
mod error;
mod login;
mod regist;
mod update;

pub use current::current as user_get;
pub use login::login as user_login;
pub use regist::regist as user_regist;
pub use update::update as user_update;

use rocket::serde::{Deserialize, Serialize};
use rocket_db_pools::sqlx;

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct User<Data> {
    user: Data,
}

#[derive(Debug, Serialize, sqlx::FromRow)]
#[serde(crate = "rocket::serde")]
pub struct Base {
    email: String,
    token: String,
    username: String,
    bio: String,
    image: String,
}

impl Base {
    pub async fn query_by_email(
        db: &mut sqlx::pool::PoolConnection<sqlx::Sqlite>,
        email: &str,
    ) -> Result<Self, sqlx::Error> {
        sqlx::query_as(
            "
                SELECT email,token,username,bio,image
                FROM users 
                WHERE email = ?",
        )
        .bind(email)
        .fetch_one(db)
        .await
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct FullData {
    #[serde(skip_deserializing, skip_serializing_if = "Option::is_none")]
    id: Option<i64>,
    email: String,
    token: String,
    username: String,
    bio: String,
    image: Option<String>,
    follows: Vec<String>,
    password: String,
}
